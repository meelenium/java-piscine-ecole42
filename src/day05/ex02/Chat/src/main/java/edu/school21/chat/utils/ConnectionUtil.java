package edu.school21.chat.utils;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public final class ConnectionUtil {

    private static final String URL = "jdbc:postgresql://localhost:5432/chat";
    private static final String USERNAME = "postgres";
    private static final String PASSWORD = "postgres";

    private static HikariDataSource hikariDataSource;

    private ConnectionUtil() { }

    public static void configureDataSource() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl(URL);
        hikariConfig.setUsername(USERNAME);
        hikariConfig.setPassword(PASSWORD);
        hikariDataSource = new HikariDataSource(hikariConfig);
    }

    public static HikariDataSource getHikariDataSource() {
        return (hikariDataSource);
    }
}
