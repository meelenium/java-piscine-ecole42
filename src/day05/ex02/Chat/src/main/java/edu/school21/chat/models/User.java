package edu.school21.chat.models;

import java.util.List;
import java.util.Objects;

public class User {

    private Long ID;
    private String login;
    private String password;
    private List<Chatroom> createRooms;
    private List<Chatroom> socializeRooms;

    public User(Long ID, String login, String password,
                List<Chatroom> createRooms, List<Chatroom> socializeRooms) {
        this.ID = ID;
        this.login = login;
        this.password = password;
        this.createRooms = createRooms;
        this.socializeRooms = socializeRooms;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setCreateRooms(List<Chatroom> createRooms) {
        this.createRooms = createRooms;
    }

    public void setSocializeRooms(List<Chatroom> socializeRooms) {
        this.socializeRooms = socializeRooms;
    }

    public Long getID() {
        return (this.ID);
    }

    public String getLogin() {
        return (this.login);
    }

    public String getPassword() {
        return (this.password);
    }

    public List<Chatroom> getCreateRooms() {
        return (this.createRooms);
    }

    public List<Chatroom> getSocializeRooms() {
        return (this.socializeRooms);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return (Objects.equals(ID, user.ID) && Objects.equals(login, user.login) && Objects.equals(password, user.password) && Objects.equals(createRooms, user.createRooms) && Objects.equals(socializeRooms, user.socializeRooms));
    }

    @Override
    public int hashCode() {
        return (Objects.hash(ID, login, password, createRooms, socializeRooms));
    }

    @Override
    public String toString() {
        return ("User{" +
                "ID=" + ID +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", createRooms=" + createRooms +
                ", socializeRooms=" + socializeRooms +
                '}');
    }
}
