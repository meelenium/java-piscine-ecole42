package edu.school21.chat.repositories;

import edu.school21.chat.models.Chatroom;
import edu.school21.chat.models.Message;
import edu.school21.chat.models.User;
import javax.sql.DataSource;
import java.sql.*;
import java.util.Optional;

public class MessagesRepositoryJdbcImpl implements MessagesRepository {

    private final String FIND_MESSAGE_BY_ID = "SELECT * " +
                                              "FROM message " +
                                              "WHERE id = ?";

    private final String FIND_USER_BY_ID = "SELECT * " +
                                           "FROM \"user\" " +
                                           "WHERE id = ?";

    private final String FIND_ROOM_BY_ID = "SELECT *" +
                                           "FROM chat_room " +
                                           "WHERE id = ?";

    DataSource dataSource;

    public MessagesRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Optional<Message> findById(Long ID) {
        try(Connection connection = this.dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(this.FIND_MESSAGE_BY_ID);
            preparedStatement.setLong(1, ID);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(!resultSet.next()) {
                return (Optional.empty());
            }

            Long messageID = resultSet.getLong(1);
            Long authorID = resultSet.getLong(2);
            Long roomID = resultSet.getLong(3);
            String text = resultSet.getString(4);
            Timestamp date_time = resultSet.getTimestamp(5);

            preparedStatement.close();
            return (Optional.of(new Message(messageID,
                                                    this.getUser(authorID),
                                                    this.getRoom(roomID),
                                                    text,
                                                    date_time)));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private User getUser(Long ID) {
        try(Connection connection = this.dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(this.FIND_USER_BY_ID);
            preparedStatement.setLong(1, ID);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(!resultSet.next()) {
                return (null);
            }

            String name = resultSet.getString(2);
            String password = resultSet.getString(3);

            preparedStatement.close();
            return (new User(ID, name, password, null, null));

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private Chatroom getRoom(Long ID) {
        try(Connection connection = this.dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(this.FIND_ROOM_BY_ID);
            preparedStatement.setLong(1, ID);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(!resultSet.next()) {
                return (null);
            }

            String roomName = resultSet.getString(2);
            Long userID = resultSet.getLong(3);
            
            preparedStatement.close();
            return (new Chatroom(ID, roomName, getUser(userID), null));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
