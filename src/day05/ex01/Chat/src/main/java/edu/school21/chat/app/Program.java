package edu.school21.chat.app;

import com.zaxxer.hikari.HikariDataSource;
import edu.school21.chat.utils.ConnectionUtil;
import edu.school21.chat.models.Message;
import edu.school21.chat.repositories.MessagesRepositoryJdbcImpl;
import java.util.Optional;
import java.util.Scanner;

public class Program {

    public static void main(String[] args) {
        ConnectionUtil.configureDataSource();
        HikariDataSource hikariDataSource = ConnectionUtil.getHikariDataSource();
        MessagesRepositoryJdbcImpl messagesRepository =
                                new MessagesRepositoryJdbcImpl(hikariDataSource);
        Scanner scanner = new Scanner(System.in);
        long messageID;

        System.out.println("Enter -1 for exit");
        while(true) {
            System.out.print("Enter a message ID: ");
            messageID = scanner.nextLong();
            if(messageID == -1) {
                System.out.println("Good bye!");
                break ;
            }
            Optional<Message> message = messagesRepository.findById(messageID);
            if(!message.isPresent()) {
                System.err.println("ERROR: Message not found");
                System.exit(-1);
            }
            System.out.println(message.get());
        }
    }
}
