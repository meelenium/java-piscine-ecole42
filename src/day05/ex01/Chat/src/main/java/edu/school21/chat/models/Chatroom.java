package edu.school21.chat.models;

import java.util.List;
import java.util.Objects;

public class Chatroom {

    private Long ID;
    private String name;
    private User owner;
    private List<Message> messages;

    public Chatroom(Long ID, String name, User owner, List<Message> messages) {
        this.ID = ID;
        this.name = name;
        this.owner = owner;
        this.messages = messages;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public Long getID() {
        return (this.ID);
    }

    public String getName() {
        return (this.name);
    }

    public User getOwner() {
        return (this.owner);
    }

    public List<Message> getMessages() {
        return (this.messages);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Chatroom chatroom = (Chatroom) o;
        return (Objects.equals(ID, chatroom.ID) && Objects.equals(name, chatroom.name) && Objects.equals(owner, chatroom.owner) && Objects.equals(messages, chatroom.messages));
    }

    @Override
    public int hashCode() {
        return (Objects.hash(ID, name, owner, messages));
    }

    @Override
    public String toString() {
        return ("Chatroom{" +
                "ID=" + ID +
                ", name='" + name + '\'' +
                ", owner=" + owner +
                ", messages=" + messages +
                '}');
    }
}
