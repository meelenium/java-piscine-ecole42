package edu.school21.chat.app;

import com.zaxxer.hikari.HikariDataSource;
import edu.school21.chat.models.Chatroom;
import edu.school21.chat.models.Message;
import edu.school21.chat.models.User;
import edu.school21.chat.utils.ConnectionUtil;
import edu.school21.chat.repositories.MessagesRepositoryJdbcImpl;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class Program {

    public static void main(String[] args) {
        ConnectionUtil.configureDataSource();
        HikariDataSource hikariDataSource = ConnectionUtil.getHikariDataSource();
        MessagesRepositoryJdbcImpl messagesRepository =
                                new MessagesRepositoryJdbcImpl(hikariDataSource);
        User user = new User(3L, "User", "User", new ArrayList<>(), new ArrayList<>());
        Chatroom room = new Chatroom(3L, "room", user, new ArrayList<>());
        Message message = new Message(4L, user, room, "Hello, world!", Timestamp.valueOf(LocalDateTime.now()));
        messagesRepository.update(message);

        message.setID(2L);
        message.setText(null);
        message.setDateTime(null);
        messagesRepository.update(message);

        /*
        must be exception
         */
//        User user2 = new User(null, "User", "User", new ArrayList<>(), new ArrayList<>());
//        Chatroom room2 = new Chatroom(null, "room", user, new ArrayList<>());
//        Message message2 = new Message(4L, user2, room2, "Hello, world!", Timestamp.valueOf(LocalDateTime.now()));
//        messagesRepository.save(message2);

//        User user3 = new User(45L, "User", "User", new ArrayList<>(), new ArrayList<>());
//        Chatroom room3 = new Chatroom(45L, "room", user, new ArrayList<>());
//        Message message3 = new Message(4L, user3, room3, "Hello, world!", Timestamp.valueOf(LocalDateTime.now()));
//        messagesRepository.save(message3);
    }
}
