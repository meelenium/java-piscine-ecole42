package edu.school21.chat.models;

import java.sql.Timestamp;
import java.util.Objects;

public class Message {

    private Long ID;
    private User author;
    private Chatroom room;
    private String text;
    private Timestamp dateTime;

    public Message(Long ID, User author, Chatroom room,
                   String text, Timestamp dateTime) {
        this.ID = ID;
        this.author = author;
        this.room = room;
        this.text = text;
        this.dateTime = dateTime;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public void setRoom(Chatroom room) {
        this.room = room;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setDateTime(Timestamp dateTime) {
        this.dateTime = dateTime;
    }

    public Long getID() {
        return (this.ID);
    }

    public User getAuthor() {
        return (this.author);
    }

    public Chatroom getRoom() {
        return (this.room);
    }

    public String getText() {
        return (this.text);
    }

    public Timestamp getDateTime() {
        return (this.dateTime);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return (Objects.equals(ID, message.ID) && Objects.equals(author, message.author) && Objects.equals(room, message.room) && Objects.equals(text, message.text) && Objects.equals(dateTime, message.dateTime));
    }

    @Override
    public int hashCode() {
        return (Objects.hash(ID, author, room, text, dateTime));
    }

    @Override
    public String toString() {
        return ("Message{" +
                "ID=" + ID +
                ", author=" + author +
                ", room=" + room +
                ", text='" + text + '\'' +
                ", dateTime=" + dateTime +
                '}');
    }
}
