INSERT INTO "user" (login, password)
                   VALUES ('Karim', 'password');
INSERT INTO "user" (login, password)
                   VALUES ('Artem', 'password');
INSERT INTO "user" (login, password)
                   VALUES ('Misha', 'password');
INSERT INTO "user" (login, password)
                   VALUES ('Masha', 'password');
INSERT INTO "user" (login, password)
                   VALUES ('Nikita', 'password');


INSERT INTO chat_room (name, owner)
                      VALUES ('KarimRoom', 1);
INSERT INTO chat_room (name, owner)
                      VALUES ('ArtemRoom', 2);
INSERT INTO chat_room (name, owner)
                      VALUES ('MishaRoom', 3);
INSERT INTO chat_room (name, owner)
                      VALUES ('MashaRoom', 4);
INSERT INTO chat_room (name, owner)
                      VALUES ('NikitaRoom', 5);


INSERT INTO message (author_id, room_id, text)
                    VALUES (1, 1, 'Hello everyone');
INSERT INTO message (author_id, room_id, text)
                    VALUES (2, 1, 'Sup');
INSERT INTO message (author_id, room_id, text)
                    VALUES (3, 3, 'Hello everyone');
INSERT INTO message (author_id, room_id, text)
                    VALUES (4, 4, 'Hello everyone');
INSERT INTO message (author_id, room_id, text)
                    VALUES (5, 5, 'Hello everyone');