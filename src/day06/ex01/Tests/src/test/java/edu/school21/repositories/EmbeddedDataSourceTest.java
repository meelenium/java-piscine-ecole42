package edu.school21.repositories;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import java.sql.SQLException;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class EmbeddedDataSourceTest {

    EmbeddedDatabase dataSource;

    @BeforeEach
    void init() {
        this.dataSource = new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.HSQL)
                .addScript("schema.sql")
                .addScript("data.sql")
                .build();
    }

    @Test
    void checkConnectionToDB() throws SQLException {
        assertNotNull(this.dataSource.getConnection());
    }

    @AfterEach
    void pullClose() {
        dataSource.shutdown();
    }
}
