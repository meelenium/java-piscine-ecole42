package edu.school21.repositories;

import edu.school21.models.Product;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProductsRepositoryJdbcImpl implements ProductsRepository {

    private final String FIND_ALL_QUERY = "SELECT * " +
                                          "FROM tests.products";

    private final String FIND_BY_ID_QUERY = "SELECT * " +
                                            "FROM tests.products " +
                                            "WHERE id = ?";

    private final String UPDATE_QUERY = "UPDATE tests.products " +
                                        "SET name = ?, price = ? " +
                                        "WHERE id = ?";

    private final String SAVE_QUERY = "INSERT INTO tests.products(id, name, price) " +
                                      "VALUES(?, ?, ?)";

    private final String DELETE_QUERY = "DELETE FROM tests.products " +
                                        "WHERE id = ?";

    private DataSource dataSource;

    public ProductsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Product> findAll() {
        try(Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement()) {
            List<Product> products = new ArrayList<>();

            ResultSet resultSet = statement.executeQuery(this.FIND_ALL_QUERY);

            while(resultSet.next()) {
                products.add(new Product(resultSet.getLong(1),
                                        resultSet.getString(2),
                                        resultSet.getInt(3)));
            }
            return (products);
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public Optional<Product> findById(Long ID) {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(this.FIND_BY_ID_QUERY)) {
            preparedStatement.setLong(1, ID);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(!resultSet.next()) {
                return (Optional.empty());
            }

            return (Optional.of(new Product(resultSet.getLong(1),
                                            resultSet.getString(2),
                                            resultSet.getInt(3))));
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void update(Product product) {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(this.UPDATE_QUERY)) {
            preparedStatement.setString(1, product.getName());
            preparedStatement.setInt(2, product.getPrice());
            preparedStatement.setLong(3, product.getID());
            preparedStatement.execute();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void save(Product product) {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(this.SAVE_QUERY)) {
            preparedStatement.setLong(1, product.getID());
            preparedStatement.setString(2, product.getName());
            preparedStatement.setInt(3, product.getPrice());
            preparedStatement.execute();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void delete(Long ID) {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(this.DELETE_QUERY)) {
            preparedStatement.setLong(1, ID);
            preparedStatement.execute();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }
}
