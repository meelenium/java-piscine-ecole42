package edu.school21.models;

import java.util.Objects;

public class Product {

    private Long ID;
    private String name;
    private Integer price;

    public Product(Long ID, String name, Integer price) {
        this.ID = ID;
        this.name = name;
        this.price = price;
    }

    public Long getID() {
        return (this.ID);
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getName() {
        return (this.name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return (this.price);
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(ID, product.ID) && Objects.equals(name, product.name) && Objects.equals(price, product.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ID, name, price);
    }

    @Override
    public String toString() {
        return "Product{" +
                "ID=" + ID +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
