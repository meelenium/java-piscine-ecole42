package edu.school21.services;

import edu.school21.exceptions.AlreadyAuthenticatedException;
import edu.school21.exceptions.EntityNotFoundException;
import edu.school21.models.User;
import edu.school21.repositories.UserRepository;
import java.util.Objects;

public class UserServiceImpl {

    private UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public boolean authenticate(String login, String password) throws AlreadyAuthenticatedException, EntityNotFoundException {
            User user = this.userRepository.findByLogin(login);

            if(Objects.isNull(user)) {
                throw new EntityNotFoundException("EXCEPTION: user not found");
            }
            if(!user.getPassword().equals(password)) {
                return (false);
            }
            if(user.isAuthenticated()) {
                throw new AlreadyAuthenticatedException("EXCEPTION: user already auth");
            }
            user.setAuthStatus(true);
            this.userRepository.update(user);
            return (true);
    }
}
