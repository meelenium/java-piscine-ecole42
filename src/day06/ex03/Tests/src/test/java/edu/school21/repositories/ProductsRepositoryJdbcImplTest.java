package edu.school21.repositories;
import edu.school21.models.Product;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ProductsRepositoryJdbcImplTest {

    final List<Product> EXPECTED_FIND_ALL_PRODUCTS = Arrays.asList(
                new Product(1L, "pizza", 1200),
                new Product(2L, "chair", 5000),
                new Product(3L, "t-shirt", 2000),
                new Product(4L, "jeans", 3000),
                new Product(5L, "boots", 12000)
            );
    final Product EXPECTED_FIND_BY_ID_PRODUCT = new Product(3L, "t-shirt", 2000);
    final Product EXPECTED_UPDATED_PRODUCT = new Product(3L, "t-shirt new season", 2000);
    final List<Product> EXPECTED_DELETE_PRODUCT = Arrays.asList(
            new Product(1L, "pizza", 1200),
            new Product(2L, "chair", 5000),
            new Product(4L, "jeans", 3000),
            new Product(5L, "boots", 12000)
    );
    final List<Product> EXPECTED_SAVE_PRODUCT = Arrays.asList(
            new Product(1L, "pizza", 1200),
            new Product(2L, "chair", 5000),
            new Product(3L, "t-shirt", 2000),
            new Product(4L, "jeans", 3000),
            new Product(5L, "boots", 12000),
            new Product(6L, "pizza", 120)
    );;

    EmbeddedDatabase dataSource;
    ProductsRepository productsRepository;

    @BeforeEach
    void init() {
        this.dataSource = new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.HSQL)
                .addScript("schema.sql")
                .addScript("data.sql")
                .build();
        productsRepository = new ProductsRepositoryJdbcImpl(this.dataSource);
    }

    @Test
    void findAllTest() {
        assertEquals(EXPECTED_FIND_ALL_PRODUCTS, productsRepository.findAll());
    }

    @Test
    void findByIDTest() {
        assertEquals(EXPECTED_FIND_BY_ID_PRODUCT, productsRepository.findById(3L).get());
        assertEquals(Optional.empty(), productsRepository.findById(6L));
    }

    @Test
    void updateTest() {
        productsRepository.update(EXPECTED_UPDATED_PRODUCT);
        Product product = productsRepository.findById(3L).get();
        assertEquals(EXPECTED_UPDATED_PRODUCT, product);
    }

    @Test
    void deleteTest() {
        productsRepository.delete(3L);
        List<Product> products = productsRepository.findAll();
        assertEquals(EXPECTED_DELETE_PRODUCT, products);
    }

    @Test
    void saveTest() {
        productsRepository.save(new Product(6L, "pizza", 120));
        List<Product> products = productsRepository.findAll();
        assertEquals(EXPECTED_SAVE_PRODUCT, products);
    }

    @AfterEach
    void pullClose() {
        this.dataSource.shutdown();
    }
}
