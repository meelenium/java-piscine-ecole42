package edu.school21.services;

import edu.school21.exceptions.AlreadyAuthenticatedException;
import edu.school21.exceptions.EntityNotFoundException;
import edu.school21.models.User;
import edu.school21.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class UserServiceImplTest {

    final String CORRECT_LOGIN = "correct_login";
    final String CORRECT_PASSWORD = "correct_password";
    final String INCORRECT_LOGIN = "incorrect_login";
    final String INCORRECT_PASSWORD = "incorrect_password";

    UserRepository userRepository;
    UserServiceImpl userService;

    User getUser(String login, String password, boolean authStatus) {
        return (new User(1L, login, password, authStatus));
    }

    @BeforeEach
    void init() {
        this.userRepository = Mockito.mock(UserRepository.class);
        this.userService = new UserServiceImpl(userRepository);
    }

    @Test
    void correctLoginPasswordCheck() throws EntityNotFoundException, AlreadyAuthenticatedException {
        User user = this.getUser(CORRECT_LOGIN, CORRECT_PASSWORD, false);
        when(this.userRepository.findByLogin(CORRECT_LOGIN))
                .thenReturn(user);
        doNothing().when(this.userRepository)
                .update(user);
        assertTrue(this.userService.authenticate(CORRECT_LOGIN, CORRECT_PASSWORD));
    }

    @Test
    void incorrectLoginCheck() throws EntityNotFoundException, AlreadyAuthenticatedException {
        when(this.userRepository.findByLogin(INCORRECT_LOGIN))
                .thenThrow(EntityNotFoundException.class);
        assertThrows(EntityNotFoundException.class, () -> this.userService.authenticate(INCORRECT_LOGIN, CORRECT_PASSWORD));
    }

    @Test
    void incorrectPasswordCheck() throws EntityNotFoundException, AlreadyAuthenticatedException {
        when(this.userRepository.findByLogin(CORRECT_LOGIN))
                .thenReturn(this.getUser(CORRECT_LOGIN, CORRECT_PASSWORD, false));
        assertFalse(this.userService.authenticate(CORRECT_LOGIN, INCORRECT_PASSWORD));
    }

    @Test
    void alreadyAuthTest() throws EntityNotFoundException {
        when(this.userRepository.findByLogin(CORRECT_LOGIN))
                .thenReturn(this.getUser(CORRECT_LOGIN, CORRECT_PASSWORD, true));
        assertThrows(AlreadyAuthenticatedException.class, () -> this.userService.authenticate(CORRECT_LOGIN, CORRECT_PASSWORD));
    }
}
