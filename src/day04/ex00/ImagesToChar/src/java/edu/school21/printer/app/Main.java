package edu.school21.printer.app;

import edu.school21.printer.logic.ImageConverter;

import java.io.File;

public class Main {

    public static void main(String[] args) {
        if(args.length != 3 || (args[0].length() > 1 || args[1].length() > 1)) {
            System.err.println("ERROR: Argument error");
            System.exit(-1);
        }
        char white = args[0].charAt(0);
        char black = args[1].charAt(0);
        File file = new File(args[2]);

        ImageConverter imageConverter = new ImageConverter(file, white, black);
        imageConverter.convertAndPrintImage();
    }
}
