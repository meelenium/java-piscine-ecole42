# Удаляем директорию target
rm -rf target

# создаем папку target
mkdir target

# Компилируем классы и закдываем class файлы в директорию target с помощью флага -d
javac src/java/edu/school21/printer/app/Main.java src/java/edu/school21/printer/logic/ImageConverter.java -d target

# Запускаем программу и с помощью флага cp указываем директорию где искать class файлы.
java -cp target edu/school21/printer/app/Main . 0 it.bmp