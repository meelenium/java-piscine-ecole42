# Удаляем директорию target
rm -rf target

# создаем директорию
mkdir target

# Компилируем классы и закдываем class файлы в директорию target с помощью флага -d
javac src/java/edu/school21/printer/app/Main.java src/java/edu/school21/printer/logic/ImageConverter.java -d target && cp -R resources target

# Собираем все файлы в JAR архив.
# Флаг c: создаем новый архив
# Флаг f: указать свое имя архива
# Флаг C: поместить все в директории target внутриь JAR архива
jar -cfm target/images-to-chars-printer.jar manifest.txt -C target .

# Запускаем JAR архив
java -jar target/images-to-chars-printer.jar . 0

