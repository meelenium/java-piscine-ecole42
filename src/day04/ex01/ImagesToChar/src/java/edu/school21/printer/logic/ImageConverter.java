package edu.school21.printer.logic;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageConverter {

    private final BufferedImage image;
    private final char white;
    private final char black;

    public ImageConverter(char white, char black) {
        try {
            this.image = ImageIO.read(ImageConverter.class.getResource("/resources/it.bmp"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        this.white = white;
        this.black = black;
    }

    public void convertAndPrintImage() {
        int width = image.getWidth();
        int height = image.getHeight();
        int color;

        for(int y = 0; y < height; ++y) {
            for(int x = 0; x < width; ++x) {
                color = image.getRGB(x, y);
                if(color == Color.WHITE.getRGB()) {
                    System.out.print(white);
                } else if(color == Color.BLACK.getRGB()) {
                    System.out.print(black);
                }
            }
            System.out.println();
        }
    }

}
