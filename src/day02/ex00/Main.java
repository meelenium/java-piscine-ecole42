package day02.ex00;

import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try(OutputStream fileOutStream = new FileOutputStream("src/Result.txt")) {
            File signatureFile = new File("src/FilesSignatures.txt");
            FileSignatureReader fileSignatureReader = new FileSignatureReader();
            Scanner scanner = new Scanner(System.in);
            String signature;
            String scannerInput;
            fileSignatureReader.loadSignaturesFromFile(signatureFile, ", ");
            while(true) {
                System.out.print("Enter path to file: ");
                scannerInput = scanner.nextLine();
                if(scannerInput.equals("42")) {
                    break ;
                }
                signature = fileSignatureReader.readFileSignature(scannerInput);
                System.out.println("PROCESSED");
                fileOutStream.write(fileSignatureReader.findSignature(signature).getBytes());
                fileOutStream.write('\n');
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}