package day02.ex00;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class FileSignatureReader {

    private Map<String, String> signatureList;

    public FileSignatureReader() {
        this.signatureList = new HashMap<>();
    }

    public void loadSignaturesFromFile(File filesSignatures, String delimiter) {
        try(Scanner scanner = new Scanner(filesSignatures)) {
            String str;
            while(scanner.hasNextLine()) {
                str = scanner.nextLine();
                this.signatureList.put(str.split(delimiter)[0], str.split(delimiter)[1]);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(-1);
        }
    }

    public String readFileSignature(String pathToFile) {
        String signature = "";
        try(InputStream fileStream = new FileInputStream(pathToFile)) {
            int symb = 0;
            int count = 0;
            while((symb = fileStream.read()) != -1 && count < 8) {
                if(Integer.toHexString(symb).length() == 1) {
                    signature += '0';
                }
                signature += Integer.toHexString(symb);
                if(count != 7) {
                    signature += ' ';
                }
                ++count;
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(-1);
        }
        return (signature.toUpperCase());
    }

    public String findSignature(String signature) {
        for(Map.Entry<String, String> map : this.signatureList.entrySet()) {
            if(map.getValue().equals(signature)) {
                return (map.getKey());
            }
        }
        return ("UNDEFINED");
    }
}
