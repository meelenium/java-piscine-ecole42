package day01.ex00;

import java.util.UUID;

public class Transaction {

    private UUID identifier;
    private User recipient;
    private User sender;
    private String transferCategory;
    private Integer transferAmount;

    public Transaction(User recipient, User sender, Integer transferAmount) {
        this.identifier = UUID.randomUUID();
        if(transferAmount < 0) {
            if(sender.getBalance() - transferAmount * -1 < 0) {
                System.out.println("На балансе недостаточно средств.");
                return ;
            }
            sender.setBalance(sender.getBalance() - transferAmount * -1);
            recipient.setBalance(recipient.getBalance() + transferAmount * -1);
            this.transferCategory = "DEBIT";
        }
        else {
            if(sender.getBalance() - transferAmount < 0) {
                System.out.println("На балансе недостаточно средств.");
                return ;
            }
            sender.setBalance(sender.getBalance() - transferAmount);
            recipient.setBalance(recipient.getBalance() + transferAmount);
            this.transferCategory = "CREDIT";
        }
        this.recipient = recipient;
        this.sender = sender;
        this.transferAmount = transferAmount;
    }

    public void setRecipient(User recipient) {
        this.recipient = recipient;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public void setTransferAmount(Integer transferAmount) {
        if(this.transferCategory.equals("DEBIT") && transferAmount < 0) {
            System.err.println("DEBIT транзакция не может быть отрицательной.");
            return ;
        } else if(this.transferCategory.equals("CREDIT") && transferAmount >= 0) {
            System.err.println("CREDIT транзакция не может быть положительной.");
            return ;
        }
        this.transferAmount = transferAmount;
    }

    public void setTransferCategory(String transferCategory) {
        if(!transferCategory.equals("DEBIT") && !transferCategory.equals("CREDIT")) {
            System.err.println(transferCategory + " - несуществующий тип транзакции.");
            return ;
        }
        this.transferCategory = transferCategory;
    }

    public UUID getIdentifier() {
        return (this.identifier);
    }

    public User getRecipient() {
        return (this.recipient);
    }

    public User getSender() {
        return (this.sender);
    }

    public String getTransferCategory() {
        return (this.transferCategory);
    }

    public Integer getTransferAmount() {
        return (this.transferAmount);
    }
}