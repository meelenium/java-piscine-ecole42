package day01.ex00;

public class Program {

    public static void main(String[] args) {
        System.out.println("User class:");
        System.out.println("\n----------------------------------");
        User user = new User(1200, "Karim");
        System.out.println("Name: " + user.getName());
        System.out.println("Balance: " + user.getBalance());
        System.out.println("----------------------------------");

        System.out.println("\n----------------------------------");
        // проверка на ошибку
        User userE = new User(-1200, "Artem");
        System.out.println("Name: " + userE.getName());
        System.out.println("Balance: " + userE.getBalance());
        System.out.println("----------------------------------");
        System.out.println("");

        System.out.println("Transaction class:");
        User karim = new User(0, "Karim");
        User artem = new User(70000, "Artem");

        System.out.println("Karim info before transaction: ");
        System.out.println("Balance: " + karim.getBalance());
        System.out.println("");

        System.out.println("Artem info before transaction: ");
        System.out.println("Balance: " + artem.getBalance());

        System.out.println("---------------------------------------");

        Transaction transaction = new Transaction(karim, artem, 10000);

        System.out.println("Karim info after transaction: ");
        System.out.println("Transaction UUID: " + transaction.getIdentifier());
        System.out.println("Balance: " + karim.getBalance());
        System.out.println("");

        System.out.println("Artem info after transaction: ");
        System.out.println("Balance: " + artem.getBalance());

        System.out.println("-----------------------------------------");
        System.out.println("");

        System.out.println("Некорректная транзакция:");
        Transaction transactionE = new Transaction(artem, karim, 11000);
        System.out.println("-----------------------------------------");
        System.out.println("Karim info after transaction: ");
        System.out.println("Transaction UUID: " + transactionE.getIdentifier());
        System.out.println("Balance: " + karim.getBalance());
        System.out.println("");

        System.out.println("Artem info after transaction: ");
        System.out.println("Balance: " + artem.getBalance());

        System.out.println("-----------------------------------------");

    }

}