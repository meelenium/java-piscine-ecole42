package day01.ex01;

public class Program {
    public static void main(String[] args) {
        User user1 = new User(100, "Karim");
        User user2 = new User(200, "Artem");
        User user3 = new User(300, "Dima");
        User user4 = new User(400, "Nikita");

        System.out.println("Name: " + user1.getName());
        System.out.println("Id: " + user1.getIdentifier());
        System.out.println("Name: " + user2.getName());
        System.out.println("Id: " + user2.getIdentifier());
        System.out.println("Name: " + user3.getName());
        System.out.println("Id: " + user3.getIdentifier());
        System.out.println("Name: " + user4.getName());
        System.out.println("Id: " + user4.getIdentifier());
    }
}