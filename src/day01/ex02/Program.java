package day01.ex02;

public class Program {
    public static void main(String[] args) {
        User user = new User(1000, "Karim");
        User user1 = new User(1000, "Artem");
        User user2 = new User(1000, "Misha");
        UserArrayList userArrayList = new UserArrayList();

        userArrayList.addUser(user);
        userArrayList.addUser(user1);
        userArrayList.addUser(user2);

        System.out.println("------------------------");
        System.out.println("Get user by id:");
        try {
            User userById = userArrayList.retriveUserById(1);
            System.out.println("Name: " + userById.getName());
            System.out.println("Balance: " + userById.getBalance());
            System.out.println("ID: " + userById.getIdentifier());
        } catch (UserNotFoundException ex) {
            ex.printStackTrace();
        }
        System.out.println("-------------------------");
        System.out.println();
        System.out.println("------------------------");
        System.out.println("Get user by index:");
        try {
            User userByIndex = userArrayList.retriveUserByIndex(1);
            System.out.println("Name: " + userByIndex.getName());
            System.out.println("Balance: " + userByIndex.getBalance());
            System.out.println("ID: " + userByIndex.getIdentifier());
        } catch (UserNotFoundException ex) {
            ex.printStackTrace();
        }
        System.out.println("------------------------");
        System.out.println();

        try {
            userArrayList.retriveUserById(4);
        } catch (UserNotFoundException ex) {
            ex.printStackTrace();
        }

        try {
            userArrayList.retriveUserByIndex(3);
        } catch (UserNotFoundException ex) {
            ex.printStackTrace();
        }

        System.out.println("Number of users: " + userArrayList.retriveNumberOfUsers());
    }
}