package day01.ex02;

public interface UserList {

    void addUser(User user);
    User retriveUserById(Integer identifier) throws UserNotFoundException;
    User retriveUserByIndex(Integer index) throws UserNotFoundException;
    Integer retriveNumberOfUsers();
}