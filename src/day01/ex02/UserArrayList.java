package day01.ex02;

public class UserArrayList implements UserList {

    private User[] userList;
    private Integer size;
    private Integer copacity;

    public UserArrayList() {
        this.size = 0;
        this.copacity = 10;
        this.userList = new User[this.copacity];
    }

    @Override
    public void addUser(User user) {
        if(this.size.equals(this.copacity)) {
            this.arrayResize();
        }
        this.userList[this.size] = user;
        ++this.size;
    }

    @Override
    public User retriveUserById(Integer identifier) throws UserNotFoundException {
        for(int i = 0; i < this.size; ++i) {
            if(this.userList[i].getIdentifier().equals(identifier)) {
                return (this.userList[i]);
            }
        }
        throw new UserNotFoundException("Exception: user's id not found");
    }

    @Override
    public User retriveUserByIndex(Integer index) throws UserNotFoundException {
        if(index > this.size - 1) {
            throw new UserNotFoundException("Exception: user's index not found");
        } else {
            return (userList[index]);
        }
    }

    @Override
    public Integer retriveNumberOfUsers() {
        return (this.size);
    }

    private void arrayResize() {
        this.copacity *= 2;
        User[] oldUserList = this.userList;
        this.userList = new User[this.copacity];
        for(int i = 0; i < this.size; ++i) {
            this.userList[i] = oldUserList[i];
        }
    }
}