package day01.ex02;

public class User {

    private Integer identifier;
    private String name;
    private Integer balance;

    public User(Integer balance, String name) {
        if(balance < 0) {
            System.err.println("Баланс не может быть отрицательным.");
            this.balance = 0;
        } else {
            this.balance = balance;
        }
        if(name == null) {
            this.name = "NoName";
        } else {
            this.name = name;
        }
        this.identifier = UseridsGenerator.getInstance().generateId();
    }

    public void setName(String name) {
        if(name == null) {
            this.name = "NoName";
        }
        else {
            this.name = name;
        }
    }

    public String getName() {
        return (this.name);
    }

    public void setBalance(Integer balance) {
        if(balance < 0) {
            System.err.println("Баланс не может быть отрицательным.");
            this.balance = 0;
        } else {
            this.balance = balance;
        }
    }

    public Integer getBalance() {
        return (this.balance);
    }

    public Integer getIdentifier() {
        return (this.identifier);
    }

}