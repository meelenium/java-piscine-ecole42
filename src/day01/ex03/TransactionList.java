package day01.ex03;

import java.util.UUID;

public interface TransactionList {
    void addTransaction(Transaction transaction);
    void deleteTransaction(UUID uuid) throws TransactionNotFoundException;
    Transaction[] transactionListToArray();
}
