package day01.ex03;

import java.util.UUID;

public class Transaction {

    private final UUID identifier;
    private final User sender;
    private final User recipient;
    private final Integer amount;
    private final TransactionType transactionType;

    private enum TransactionType {
        DEBIT,
        CREDIT
    }

    public Transaction(User sender, User recipient, Integer amount) {
        this.identifier = UUID.randomUUID();
        this.recipient = recipient;
        this.sender = sender;
        this.amount = amount;
        if(amount < 0) {
            transactionType = TransactionType.CREDIT;
        } else {
            transactionType = TransactionType.DEBIT;
        }
    }

    public UUID getIdentifier() {
        return (identifier);
    }

    public User getSender() {
        return (sender);
    }

    public User getRecipient() {
        return (recipient);
    }

    public Integer getAmount() {
        return (amount);
    }

    public TransactionType getTransactionType() {
        return (transactionType);
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "identifier=" + identifier +
                ", sender=" + sender.getName() +
                ", recipient=" + recipient.getName() +
                ", amount=" + amount +
                ", transactionType=" + transactionType +
                '}';
    }
}
