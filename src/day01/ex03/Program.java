package day01.ex03;

import java.util.UUID;

public class Program {
    public static void main(String[] args) {
        User user = new User(1000, "Karim");
        User user1 = new User(1000, "Artem");
        UUID uuid = UUID.randomUUID();
        Transaction transaction = new Transaction(user1, user, 2000);
        Transaction transaction2 = new Transaction(user, user1, 1000);
        Transaction transaction3 = new Transaction(user, user1, 1000);
        Transaction transaction4 = new Transaction(user1, user, 1000);
        Transaction transaction5 = new Transaction(user1, user, -1000);
        TransactionLinkedList transactionLinkedList = new TransactionLinkedList();

        transactionLinkedList.addTransaction(transaction);
        transactionLinkedList.addTransaction(transaction2);
        transactionLinkedList.addTransaction(transaction3);
        transactionLinkedList.addTransaction(transaction4);
        transactionLinkedList.addTransaction(transaction5);

        try {
            transactionLinkedList.deleteTransaction(transaction5.getIdentifier());
        } catch (TransactionNotFoundException ex) {
            System.err.println(ex.getLocalizedMessage());
        }

//        try {
//            transactionLinkedList.deleteTransaction(uuid);
//        } catch (TransactionNotFoundException ex) {
//            System.err.println(ex.getLocalizedMessage());
//        }

        Transaction[] transactions = transactionLinkedList.transactionListToArray();
        for(int i = 0; i < transactions.length; ++i) {
            System.out.println("----------------------------");
            System.out.println("UUID: " + transactions[i].getIdentifier());
            System.out.println("Sender: " + transactions[i].getSender().getName());
            System.out.println("Recipient: " + transactions[i].getRecipient().getName());
            System.out.println("Amount: " + transactions[i].getAmount());
            System.out.println("Transaction type: " + transactions[i].getTransactionType());
            System.out.println("----------------------------");
            System.out.println();
        }

    }
}
