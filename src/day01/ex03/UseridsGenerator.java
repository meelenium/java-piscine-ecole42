package day01.ex03;

public class UseridsGenerator {

    private static Integer id;
    private static UseridsGenerator idClass;

    private UseridsGenerator() { }

    public static UseridsGenerator getInstance() {
        if(idClass == null) {
            id = 0;
            idClass = new UseridsGenerator();
        }
        return (idClass);
    }

    public Integer generateId() {
        ++this.id;
        return (id);
    }
}