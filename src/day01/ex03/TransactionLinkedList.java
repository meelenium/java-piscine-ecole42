package day01.ex03;

import java.util.UUID;

public class TransactionLinkedList implements TransactionList {

    private LinkedList head;
    private LinkedList tail;
    private Integer countOfTransactions;

    public TransactionLinkedList() {
        this.countOfTransactions = 0;
        this.head = null;
        this.tail = null;
    }

    @Override
    public void addTransaction(Transaction transaction) {
        LinkedList tmp = new LinkedList(transaction);
        if(head == null) {
            this.tail = tmp;
            this.head = tmp;
        } else {
            this.tail.next = tmp;
            this.tail = this.tail.next;
        }
        ++this.countOfTransactions;
    }

    @Override
    public void deleteTransaction(UUID uuid) throws TransactionNotFoundException {
        if(this.head != null && this.head.data.getIdentifier().equals(uuid)) {
            this.head = this.head.next;
            --this.countOfTransactions;
            return ;
        }
        LinkedList tmp = this.head;
        LinkedList prev = this.head;
        while(tmp != null) {
            if(tmp.data.getIdentifier().equals(uuid)) {
                prev.next = tmp.next;
                --this.countOfTransactions;
                return ;
            }
            prev = tmp;
            tmp = tmp.next;
        }
        throw new TransactionNotFoundException("Exception: Transaction not found");
    }

    @Override
    public Transaction[] transactionListToArray() {
        Transaction[] transactions = new Transaction[this.countOfTransactions];
        LinkedList tmp = this.head;
        for(int i = 0; tmp != null; ++i) {
            transactions[i] = tmp.data;
            tmp = tmp.next;
        }
        return (transactions);
    }

    @Override
    public String toString() {
        LinkedList tmpList = head;
        while(tmpList != null) {
            System.out.println(tmpList.data);
            tmpList = tmpList.next;
        }
        return ("");
    }

    private static class LinkedList {
        Transaction data;
        LinkedList next;

        LinkedList(Transaction data) {
            this.data = data;
            this.next = null;
        }
    }
}
