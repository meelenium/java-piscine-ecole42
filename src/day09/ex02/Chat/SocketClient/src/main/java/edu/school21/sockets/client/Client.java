package edu.school21.sockets.client;

import java.io.*;
import java.net.Socket;

public class Client {

    BufferedReader in;
    BufferedWriter out;
    ClientReader clientReader;
    ClientWriter clientWriter;
    Socket socket;

    public Client(String host, int port) {
        try {
            this.socket = new Socket(host, port);
            this.in = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
            this.out = new BufferedWriter(new OutputStreamWriter(this.socket.getOutputStream()));
            this.clientReader = new ClientReader(this.in, this.out, this.socket);
            this.clientWriter = new ClientWriter(this.in, this.out, this.socket);
        } catch (IOException ex) {
            System.err.println("EXCEPTION: " + ex.getMessage());
            close(this.in, this.out, this.socket);
        }
    }

    public void start() {
        this.clientReader.start();
        this.clientWriter.start();
    }

    synchronized static void close(BufferedReader in, BufferedWriter out, Socket socket) {
        try {
            if (in != null) {
                in.close();
            }
            if(out != null) {
                out.close();
            }
            if(socket != null) {
                socket.close();
            }
        } catch (IOException ex) {
            System.err.println("EXCEPTION: " + ex.getMessage());
        }
        System.exit(0);
    }

}
