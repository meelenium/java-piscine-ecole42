package edu.school21.sockets.models;

import java.sql.Timestamp;
import java.util.Objects;

public class Message {

    private Long id;
    private Long sender;
    private Long room_id;
    private String text;
    private Timestamp time;

    public Message(Long id, Long sender, Long room_id, String text, Timestamp time) {
        this.id = id;
        this.sender = sender;
        this.text = text;
        this.time = time;
        this.room_id = room_id;
    }

    public Long getId() {
        return (this.id);
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSender() {
        return (this.sender);
    }

    public void setSender(Long sender) {
        this.sender = sender;
    }

    public Long getRoom_id() {
        return room_id;
    }

    public void setRoom_id(Long room_id) {
        this.room_id = room_id;
    }

    public String getText() {
        return (this.text);
    }

    public void setText(String text) {
        this.text = text;
    }

    public Timestamp getTime() {
        return (this.time);
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return Objects.equals(id, message.id) && Objects.equals(sender, message.sender) && Objects.equals(text, message.text) && Objects.equals(time, message.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, sender, text, time);
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", sender=" + sender +
                ", text='" + text + '\'' +
                ", time=" + time +
                '}';
    }
}
