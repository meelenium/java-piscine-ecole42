package edu.school21.sockets.services;

import edu.school21.sockets.models.Room;
import edu.school21.sockets.models.User;

public interface MessageService {

    void saveMessage(User sender, Room room, String text);

}
