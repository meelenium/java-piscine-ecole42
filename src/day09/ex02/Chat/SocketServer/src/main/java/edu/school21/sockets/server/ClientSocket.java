package edu.school21.sockets.server;

import edu.school21.sockets.exceptions.RoomAlreadyExistException;
import edu.school21.sockets.exceptions.UserAlreadyExistException;
import edu.school21.sockets.exceptions.UserNotFoundException;
import edu.school21.sockets.models.Room;
import edu.school21.sockets.models.User;
import edu.school21.sockets.services.MessageService;
import edu.school21.sockets.services.RoomService;
import edu.school21.sockets.services.UserService;

import java.io.*;
import java.net.Socket;
import java.util.List;

class ClientSocket extends Thread {

    private Socket clientSocket;
    private BufferedReader in;
    private BufferedWriter out;
    private UserService userService;
    private MessageService messageService;
    private RoomService roomService;
    private boolean clientStatus;
    private boolean authStatus;
    private User user;
    private Room room;
    private Server server;

    public ClientSocket(Socket clientSocket, UserService userService,
                        MessageService messageService, RoomService roomService, Server server) {
        this.user = null;
        this.clientSocket = clientSocket;
        this.userService = userService;
        this.messageService = messageService;
        this.roomService = roomService;
        this.clientStatus = true;
        this.authStatus = false;
        this.server = server;
        try {
            this.in = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));
            this.out = new BufferedWriter(new OutputStreamWriter(this.clientSocket.getOutputStream()));
        } catch (IOException ex) {
            System.err.println("EXCEPTION: " + ex.getMessage());
            this.close();
        }
    }

    @Override
    public void run() {
        this.showInterface();
    }

    public String getUsername() {
        return (this.user.getUsername());
    }

    public boolean getAuthStatus() {
        return (this.authStatus);
    }

    public Room getRoom() {
        return (this.room);
    }

    private void showInterface() {
        String action;
        this.sendMsg("Hello from server!");
        while(this.clientStatus) {
            this.sendMsg("Choose an action:");
            this.sendMsg("\t1. signUp");
            this.sendMsg("\t2. signIn");
            this.sendMsg("\t3. Exit");

            action = this.receiveMsg();
            if(action == null) {
                this.close();
                break ;
            }
            System.out.println("Client choose action: " + action);
            switch (action) {
                case "1":
                    this.signUp();
                    break;
                case "2":
                    this.signIn();
                    break ;
                case "3":
                    this.exit();
                    break;
                default:
                    this.sendMsg("ERROR: Action doesn't exist");
                    System.err.println("Client selected a non-existent action");
            }
        }
    }

    private void signUp() {
        String username, password;

        this.sendMsg("Enter username: WITHOUT");
        username = receiveMsg();
        if(username == null) {
            this.close();
            return ;
        }

        this.sendMsg("Enter password: WITHOUT");
        password = receiveMsg();
        if(password == null) {
            this.close();
            return ;
        }

        try {
            this.userService.signUp(username, password);
            this.sendMsg("Successful!");
            System.out.println(username + " has registered");
        } catch (UserAlreadyExistException ex) {
            this.sendMsg(ex.getMessage());
            System.err.println("Client can't signUp");
        }
    }

    private void signIn() {
        String username, password;

        this.sendMsg("Enter username: WITHOUT");
        username = receiveMsg();
        if(username == null) {
            this.close();
            return ;
        }

        this.sendMsg("Enter password: WITHOUT");
        password = receiveMsg();
        if(password == null) {
            this.close();
            return ;
        }

        try {
            this.user = this.userService.signIn(username, password);
            System.out.println(this.user.getUsername() + " has authorized");
            this.authStatus = true;
            this.showRoomInterface();
        } catch (UserNotFoundException ex) {
            this.sendMsg(ex.getMessage());
            this.sendMsg("EXIT");
            this.close();
            System.out.println("Client under the username " + "\"" + username + "\"" + " couldn't signIn");
        }
    }

    private void showRoomInterface() {
        String action;

        while(this.clientStatus) {
            sendMsg("Choose an action");
            sendMsg("\t1. Create room");
            sendMsg("\t2. Choose room");
            sendMsg("\t3. Exit");

            action = receiveMsg();
            if(action == null) {
                this.close();
                break ;
            }
            switch (action) {
                case "1":
                    this.createRoom();
                    break ;
                case "2":
                    this.choseRoom();
                    break ;
                case "3":
                    this.exit();
                    break ;
                default:
                    this.sendMsg("ERROR: Action doesn't exist");
                    System.err.println("Client selected a non-existent action");
            }
        }
    }

    private void createRoom() {
        String roomName;

        this.sendMsg("Enter room name: WITHOUT");
        roomName = receiveMsg();
        if(roomName == null) {
            this.close();
            return ;
        }

        try {
            this.roomService.createRoom(roomName);
            this.sendMsg("Successful!");
        } catch (RoomAlreadyExistException ex) {
            this.sendMsg(ex.getMessage());
            System.err.println(this.user.getUsername() + " couldn't create room under the name " + "\"" + roomName + "\"");
        }
    }

    private void choseRoom() {
        String action;
        int roomId;

        List<Room> rooms = this.roomService.getAllRooms();
        if(rooms.isEmpty()) {
            sendMsg("There are no rooms yet");
            return ;
        }
        rooms.forEach(room -> this.sendMsg((room.getId()) + ". " + room.getName()));
        this.sendMsg((rooms.size() + 1) + ". Exit");

        action = receiveMsg();
        if (action == null) {
            this.close();
            return ;
        }
        roomId = Integer.parseInt(action);
        if(roomId == rooms.size() + 1) {
            return ;
        }
        if (roomId > rooms.size() || roomId < 1) {
            sendMsg("ERROR: room doesn't exist");
            System.err.println(this.user.getUsername() + " choose a non-existent room");
            return ;
        }
        this.room = rooms.get(roomId - 1);
        this.chat();
    }

    private void chat() {
        String msg;

        this.sendMsg("Welcome " + this.user.getUsername() + ".");
        this.sendMsg("You can write message other users here.");
        this.sendMsg("If you wanna leave the room you need write \"Exit\"");
        while(true) {
            msg = this.receiveMsg();
            if(msg == null) {
                this.close();
                break ;
            }
            if(msg.equals("Exit")) {
                this.server.sendMsgAllUsers(this.getUsername(), this.room,
                        this.getUsername() + " has left the room");
                this.sendMsg("You have left the room");
                break ;
            }
            this.messageService.saveMessage(this.user, this.room, msg);
            this.server.sendMsgAllUsers(this.getUsername(), this.room,
                        this.getUsername() + "> " + msg);
            System.out.println(this.user.getUsername() + " write the message: " + msg);
        }

    }

    private void exit() {
        this.sendMsg("You have left the chat.");
        this.sendMsg("EXIT");
        this.close();
    }

    public void close() {
        try {
            if (this.clientSocket != null) {
                this.clientSocket.close();
            }
            if(this.in != null) {
                this.in.close();
            }
            if(this.out != null) {
                this.out.close();
            }
        } catch (IOException ex) {
            System.err.println("EXCEPTION: " + ex.getMessage());
        }
        this.clientStatus = false;
        this.server.removeClient(this);
    }

    private String receiveMsg() {
        try {
            return (this.in.readLine());
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            this.close();
        }
        return (null);
    }

    void sendMsg(String message) {
        try {
            this.out.write(message + "\n");
            this.out.flush();
        } catch (IOException ex) {
            System.err.println("EXCEPTION: " + ex.getMessage());
            this.close();
        }
    }

}
