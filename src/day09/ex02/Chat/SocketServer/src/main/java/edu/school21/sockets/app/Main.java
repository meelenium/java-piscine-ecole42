package edu.school21.sockets.app;

import com.zaxxer.hikari.HikariDataSource;
import edu.school21.sockets.server.Server;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class Main {

    public static void main(String[] args) {
        if(args.length != 1) {
            System.err.println("Argument error");
            System.exit(-1);
        }

        int port = parseArg("--port=", args[0]);

        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext("edu.school21.sockets");

        Server server = context.getBean("server", Server.class);
        initUserTable(context.getBean("hikariDS", HikariDataSource.class));
        initRoomTable(context.getBean("hikariDS", HikariDataSource.class));
        initMessageTable(context.getBean("hikariDS", HikariDataSource.class));
        server.start(port);
    }

    private static int parseArg(String startsWith, String arg) {
        if(!arg.startsWith(startsWith)) {
            System.err.println("Argument error");
            System.exit(-1);
        }

        return (Integer.parseInt(arg.substring(startsWith.length(), arg.length() - 1)));
    }

    private static void initUserTable(DataSource dataSource) {
        String request = "CREATE TABLE IF NOT EXISTS \"user\" (" +
                            "id SERIAL PRIMARY KEY," +
                            "username TEXT," +
                            "password TEXT" +
                         ")";
        try {
            Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            statement.execute(request);
            connection.close();
            statement.close();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    private static void initRoomTable(DataSource dataSource) {
        String request = "CREATE TABLE IF NOT EXISTS room (" +
                "id SERIAL PRIMARY KEY," +
                "name TEXT" +
                ")";
        try {
            Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            statement.execute(request);
            connection.close();
            statement.close();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    private static void initMessageTable(DataSource dataSource) {
        String request = "CREATE TABLE IF NOT EXISTS message (" +
                            "id SERIAL PRIMARY KEY," +
                            "sender BIGINT," +
                            "room_id BIGINT," +
                            "text TEXT," +
                            "time TIMESTAMP," +
                            "FOREIGN KEY (sender) REFERENCES \"user\"(id)," +
                            "FOREIGN KEY (room_id) REFERENCES room(id)" +
                         ")";
        try {
            Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            statement.execute(request);
            connection.close();
            statement.close();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

}
