package edu.school21.sockets.repositories;

import java.util.List;

public interface CrudRepository<T> {

    T findById(Long ID);
    List<T> findAll();
    void save(T entity);
    void update(T entity);
    void delete(Long ID);

}
