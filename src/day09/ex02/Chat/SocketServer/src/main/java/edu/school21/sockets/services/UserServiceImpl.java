package edu.school21.sockets.services;

import edu.school21.sockets.exceptions.UserAlreadyExistException;
import edu.school21.sockets.exceptions.UserNotFoundException;
import edu.school21.sockets.models.User;
import edu.school21.sockets.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component("userService")
public class UserServiceImpl implements UserService {

    private PasswordEncoder passwordEncoder;
    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(@Qualifier("userRepository") UserRepository userRepository,
                          @Qualifier("passwordEncoder") PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void signUp(String username, String password) throws UserAlreadyExistException {
        if(this.isUserExist(username)) {
            throw new UserAlreadyExistException("ERROR: Username " + "\"" + username + "\"" + " already exist");
        }

        String encodedPassword = passwordEncoder.encode(password);

        userRepository.save(new User(null, username, encodedPassword));
    }

    @Override
    public User signIn(String username, String password) throws UserNotFoundException {
        Optional<User> user = this.userRepository.findByUsername(username);
        if(!user.isPresent() || !this.passwordEncoder.matches(password, user.get().getPassword())) {
            throw new UserNotFoundException("Wrong login or password");
        }
        return (user.get());
    }

    private boolean isUserExist(String username) {
        return (userRepository.findByUsername(username)
                .isPresent());
    }
}
