package edu.school21.sockets.repositories;

import edu.school21.sockets.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;
import java.util.Optional;

@Component("userRepository")
public class UserRepositoryImpl implements UserRepository {

    private final String FIND_BY_ID_QUERY = "SELECT * " +
                                            "FROM \"user\" " +
                                            "WHERE id = ?";

    private final String FIND_ALL_QUERY = "SELECT * " +
                                          "FROM \"user\"";

    private final String SAVE_QUERY = "INSERT INTO \"user\"(username, password)" +
                                      "VALUES(?, ?)";

    private final String UPDATE_QUERY = "UPDATE \"user\" " +
                                        "SET username = ?, password = ? " +
                                        "WHERE id = ?";

    private final String DELETE_QUERY = "DELETE FROM \"user\" " +
                                        "WHERE id = ?";

    private final String FIND_BY_LOGIN_QUERY = "SELECT * " +
                                               "FROM \"user\" " +
                                               "WHERE username = ? ";


    private JdbcTemplate jdbcTemplate;

    @Autowired
    public UserRepositoryImpl(@Qualifier("hikariDS") DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public User findById(Long ID) {
        return (this.jdbcTemplate.query(FIND_BY_ID_QUERY, new Object[]{ID},
                        new BeanPropertyRowMapper<>(User.class))
                .stream()
                .findAny()
                .orElse(null));
    }

    @Override
    public List<User> findAll() {
        return (this.jdbcTemplate.query(FIND_ALL_QUERY,
                new BeanPropertyRowMapper<>(User.class)));
    }

    @Override
    public void save(User entity) {
        this.jdbcTemplate.update(SAVE_QUERY, entity.getUsername(), entity.getPassword());
    }

    @Override
    public void update(User entity) {
        this.jdbcTemplate.update(UPDATE_QUERY, entity.getUsername(), entity.getPassword(), entity.getId());
    }

    @Override
    public void delete(Long ID) {
        this.jdbcTemplate.update(DELETE_QUERY, ID);
    }

    @Override
    public Optional<User> findByUsername(String username) {
        return (this.jdbcTemplate.query(FIND_BY_LOGIN_QUERY, new Object[]{username},
                        new BeanPropertyRowMapper<>(User.class))
                .stream()
                .findAny());
    }
}
