package edu.school21.sockets.exceptions;

public class RoomAlreadyExistException extends Exception {

    public RoomAlreadyExistException(String message) {
        super(message);
    }
}
