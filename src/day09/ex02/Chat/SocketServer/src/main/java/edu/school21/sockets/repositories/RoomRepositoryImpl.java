package edu.school21.sockets.repositories;

import edu.school21.sockets.models.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;
import java.util.Optional;

@Component("roomRepository")
public class RoomRepositoryImpl implements RoomRepository {

    private final String FIND_BY_ID_QUERY = "SELECT * " +
                                            "FROM room " +
                                            "WHERE id = ?";

    private final String FIND_ALL_QUERY = "SELECT * " +
                                          "FROM room";

    private final String SAVE_QUERY = "INSERT INTO room(name) " +
                                      "VALUES(?)";

    private final String UPDATE_QUERY = "UPDATE room " +
                                        "SET name = ? " +
                                        "WHERE id = ?";

    private final String DELETE_QUERY = "DELETE FROM room " +
                                        "WHERE id = ?";

    private final String FIND_BY_NAME_QUERY = "SELECT * " +
                                              "FROM room " +
                                              "WHERE name = ?";

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public RoomRepositoryImpl(@Qualifier("hikariDS") DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public Room findById(Long ID) {
        return (this.jdbcTemplate.query(FIND_BY_ID_QUERY, new Object[]{ID},
                        new BeanPropertyRowMapper<>(Room.class))
                .stream()
                .findAny()
                .orElse(null));
    }

    @Override
    public List<Room> findAll() {
        return (this.jdbcTemplate.query(FIND_ALL_QUERY,
                new BeanPropertyRowMapper<>(Room.class)));
    }

    @Override
    public void save(Room entity) {
        this.jdbcTemplate.update(SAVE_QUERY, entity.getName());
    }

    @Override
    public void update(Room entity) {
        this.jdbcTemplate.update(UPDATE_QUERY, entity.getName(), entity.getId());
    }

    @Override
    public void delete(Long ID) {
        this.jdbcTemplate.update(DELETE_QUERY, ID);
    }

    @Override
    public Optional<Room> findByName(String name) {
        return (this.jdbcTemplate.query(FIND_BY_NAME_QUERY, new Object[]{name},
                        new BeanPropertyRowMapper<>(Room.class))
                .stream()
                .findAny());
    }
}
