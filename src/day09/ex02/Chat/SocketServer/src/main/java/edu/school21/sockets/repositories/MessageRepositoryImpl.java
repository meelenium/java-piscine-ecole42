package edu.school21.sockets.repositories;

import edu.school21.sockets.models.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;

@Component("messageRepository")
public class MessageRepositoryImpl implements MessageRepository {

    private final String FIND_BY_ID_QUERY = "SELECT * " +
                                            "FROM message " +
                                            "WHERE id = ?";

    private final String FIND_ALL_QUERY = "SELECT * " +
                                          "FROM message";

    private final String SAVE_QUERY = "INSERT INTO message(sender, room_id, text, time)" +
                                                          "VALUES(?, ?, ?, ?)";

    private final String UPDATE_QUERY = "UPDATE message " +
                                        "SET sender = ?, room_id = ?, text = ?, time = ?" +
                                        "WHERE id = ?";

    private final String DELETE_QUERY = "DELETE FROM message " +
                                        "WHERE id = ?";

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public MessageRepositoryImpl(@Qualifier("hikariDS") DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }


    @Override
    public Message findById(Long ID) {
        return (this.jdbcTemplate.query(FIND_BY_ID_QUERY, new Object[]{ID},
                        new BeanPropertyRowMapper<>(Message.class))
                .stream()
                .findAny()
                .orElse(null));
    }

    @Override
    public List<Message> findAll() {
        return (this.jdbcTemplate.query(FIND_ALL_QUERY,
                new BeanPropertyRowMapper<>(Message.class)));
    }

    @Override
    public void save(Message entity) {
        this.jdbcTemplate.update(SAVE_QUERY, entity.getSender(), entity.getRoom_id(), entity.getText(),
                                            entity.getTime());
    }

    @Override
    public void update(Message entity) {
        this.jdbcTemplate.update(UPDATE_QUERY, entity.getSender(), entity.getRoom_id(), entity.getText(),
                                                entity.getTime());
    }

    @Override
    public void delete(Long ID) {
        this.jdbcTemplate.update(DELETE_QUERY, ID);
    }
}
