package edu.school21.sockets.models;

import java.util.Objects;

public class Room {

    private Long id;
    private String name;

    public Room(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Room() { }

    public Long getId() {
        return (this.id);
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return (this.name);
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Room room = (Room) o;
        return Objects.equals(id, room.id) && Objects.equals(name, room.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return "Room{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
