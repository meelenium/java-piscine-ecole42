package edu.school21.sockets.services;

import edu.school21.sockets.exceptions.RoomAlreadyExistException;
import edu.school21.sockets.models.Room;
import edu.school21.sockets.repositories.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component("roomService")
public class RoomServiceImpl implements RoomService {

    private RoomRepository roomRepository;

    @Autowired
    public RoomServiceImpl(@Qualifier("roomRepository") RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    @Override
    public void createRoom(String name) throws RoomAlreadyExistException {
        Optional<Room> roomCheck = this.roomRepository.findByName(name);
        if(roomCheck.isPresent()) {
            throw new RoomAlreadyExistException("Room under the name " + "\"" + name + "\"" + " already exist");
        }
        this.roomRepository.save(new Room(null, name));
    }

    @Override
    public List<Room> getAllRooms() {
        return (this.roomRepository.findAll());
    }
}
