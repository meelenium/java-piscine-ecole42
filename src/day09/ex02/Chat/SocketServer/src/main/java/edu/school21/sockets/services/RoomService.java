package edu.school21.sockets.services;

import edu.school21.sockets.exceptions.RoomAlreadyExistException;
import edu.school21.sockets.models.Room;

import java.util.List;

public interface RoomService {

    void createRoom(String name) throws RoomAlreadyExistException;
    List<Room> getAllRooms();

}
