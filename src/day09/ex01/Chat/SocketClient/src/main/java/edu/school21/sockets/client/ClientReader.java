package edu.school21.sockets.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.net.Socket;

class ClientReader extends Thread {

    private Socket socket;
    private BufferedReader in;
    private BufferedWriter out;

    public ClientReader(BufferedReader in, BufferedWriter out, Socket socket) {
        this.socket = socket;
        this.in = in;
        this.out = out;
    }

    @Override
    public void run() {
        StringBuilder msg = new StringBuilder();
        String tmp;

        try {
            while(true) {
                tmp = this.in.readLine();
                if(tmp == null) {
                    System.err.println("Failed to connect to server");
                    Client.close(this.in, this.out, this.socket);
                }
                if(tmp.equals("EXIT")) {
                    Client.close(this.in, this.out, this.socket);
                }
                msg.append(tmp);
                if(msg.toString().endsWith("WITHOUT")) {
                    System.out.print(msg.toString().replace("WITHOUT", ""));
                } else {
                    System.out.println(msg);
                }
                msg.setLength(0);
            }
        } catch (IOException ex) {
            System.err.println("EXCEPTION: " + ex.getMessage());
            Client.close(this.in, this.out, this.socket);
        }
    }
}
