package edu.school21.sockets.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class ClientWriter extends Thread {

    private Socket socket;
    private BufferedReader in;
    private BufferedWriter out;

    public ClientWriter(BufferedReader in, BufferedWriter out, Socket socket) {
        this.socket = socket;
        this.in = in;
        this.out = out;
    }

    @Override
    public void run() {
        Scanner scanner = new Scanner(System.in);
        String msg;

        while(true) {
            try {
                msg = scanner.nextLine();
                this.out.write(msg + "\n");
                this.out.flush();
            } catch (IOException ex) {
                System.err.println("EXCEPTION: " + ex.getMessage());
                Client.close(this.in, this.out, this.socket);
            }
        }
    }

}
