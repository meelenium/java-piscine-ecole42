package edu.school21.sockets.services;

import edu.school21.sockets.exceptions.UserAlreadyExistException;
import edu.school21.sockets.exceptions.UserNotFoundException;
import edu.school21.sockets.models.User;

public interface UserService {

    void signUp(String username, String password) throws UserAlreadyExistException;
    User signIn(String username, String password) throws UserNotFoundException;

}
