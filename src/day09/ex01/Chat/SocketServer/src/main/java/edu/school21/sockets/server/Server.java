package edu.school21.sockets.server;

import edu.school21.sockets.services.MessageService;
import edu.school21.sockets.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;

@Component("server")
public class Server {

    private ServerSocket serverSocket;
    private List<ClientSocket> clientSocket;
    private UserService userService;
    private MessageService messageService;

    @Autowired
    public Server(@Qualifier("userService") UserService userService,
                  @Qualifier("messageService") MessageService messageService) {
        this.userService = userService;
        this.messageService = messageService;
        this.clientSocket = new ArrayList<>();
    }

    public void start(int port) {
        try {
            this.serverSocket = new ServerSocket(port);
            System.out.println("CHAT LOGS:");
            while(true) {
                ClientSocket client = new ClientSocket(this.serverSocket.accept(), this.userService,
                                                        this.messageService, this);
                client.start();
                this.clientSocket.add(client);
            }
        } catch (IOException ex) {
            System.err.println("EXCEPTION: " + ex.getMessage());
            this.close();
        }
    }

    void removeClient(ClientSocket client) {
        this.clientSocket.remove(client);
    }

    void sendMsgAllUsers(String username, String message) {
        this.clientSocket.stream()
                .filter(ClientSocket::getAuthStatus)
                .filter(c -> !c.getUsername().equals(username))
                .forEach(c -> c.sendMsg(message));
    }

    private void close() {
        try {
            if (this.serverSocket != null) {
                this.serverSocket.close();
            }
        } catch (IOException ex) {
            System.err.println("EXCEPTION: " + ex.getMessage());
        }
        if(clientSocket != null) {
            this.clientSocket
                    .forEach(ClientSocket::close);
        }
        System.exit(0);
    }

}
