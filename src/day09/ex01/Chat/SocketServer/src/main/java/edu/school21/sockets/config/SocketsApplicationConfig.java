package edu.school21.sockets.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@PropertySource("classpath:db.properties")
public class SocketsApplicationConfig {

    @Value("${db.driver}")
    private String classPath;

    @Value("${db.url}")
    private String dbUrl;

    @Value("${db.user}")
    private String username;

    @Value("${db.password}")
    private String password;

    @Bean
    public HikariConfig hikariConfig() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(this.classPath);
        hikariConfig.setJdbcUrl(this.dbUrl);
        hikariConfig.setUsername(this.username);
        hikariConfig.setPassword(this.password);
        return (hikariConfig);
    }

    @Bean("hikariDS")
    public HikariDataSource hikariDataSource() {
        return (new HikariDataSource(this.hikariConfig()));
    }

    @Bean("passwordEncoder")
    public PasswordEncoder passwordEncoder() {
        return (new BCryptPasswordEncoder());
    }
}
