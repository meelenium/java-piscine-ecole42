package edu.school21.sockets.services;

import edu.school21.sockets.exceptions.UserAlreadyExistException;

public interface UserService {

    void signUp(String login, String password) throws UserAlreadyExistException;

}
