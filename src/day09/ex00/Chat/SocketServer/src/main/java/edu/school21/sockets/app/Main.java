package edu.school21.sockets.app;

import com.zaxxer.hikari.HikariDataSource;
import edu.school21.sockets.server.Server;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class Main {

    public static void main(String[] args) {
        if(args.length != 1) {
            System.err.println("Argument error");
            System.exit(-1);
        }

        int port = parseArg("--port=", args[0]);

        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext("edu.school21.sockets");

        Server server = context.getBean("server", Server.class);
        initTable(context.getBean("hikariDS", HikariDataSource.class));
        server.start(port);
    }

    private static int parseArg(String startsWith, String arg) {
        if(!arg.startsWith(startsWith)) {
            System.err.println("Argument error");
            System.exit(-1);
        }

        return (Integer.parseInt(arg.substring(startsWith.length(), arg.length() - 1)));
    }

    private static void initTable(DataSource dataSource) {
        String request = "CREATE TABLE IF NOT EXISTS \"user\" (" +
                "id SERIAL PRIMARY KEY," +
                "username TEXT," +
                "password TEXT" +
                ")";
        try {
            Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            statement.execute(request);
            connection.close();
            statement.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
