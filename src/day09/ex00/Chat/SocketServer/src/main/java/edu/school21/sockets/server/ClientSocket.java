package edu.school21.sockets.server;

import edu.school21.sockets.exceptions.UserAlreadyExistException;
import edu.school21.sockets.models.User;
import edu.school21.sockets.services.UserService;

import java.io.*;
import java.net.Socket;

class ClientSocket {

    private Socket clientSocket;
    private BufferedReader in;
    private BufferedWriter out;
    private UserService userService;
    private User user;

    public ClientSocket(Socket clientSocket, UserService userService) {
        this.clientSocket = clientSocket;
        try {
            this.in = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));
            this.out = new BufferedWriter(new OutputStreamWriter(this.clientSocket.getOutputStream()));
            this.userService = userService;
        } catch (IOException ex) {
            System.err.println("EXCEPTION: " + ex.getMessage());
            this.close();
        }
    }

    public void start() {
        String action;

        sendMsg("Hello from server!");
        sendMsg("Choose an action:");
        sendMsg("\t1. signUp");
        sendMsg("END");

        action = receiveMsg();
        if(action == null) {
            this.close();
            return ;
        }
        switch (action) {
            case "signUp":
                this.register();
                break ;
            default:
                sendMsg("ERROR: Action doesn't exist");
                sendMsg("EXIT");
        }
        this.close();
    }

    private void register() {
        String username, password;

        sendMsg("Enter username: ");
        sendMsg("END");
        username = receiveMsg();
        if(username == null) {
            this.close();
            return ;
        }

        sendMsg("Enter password: ");
        sendMsg("END");
        password = receiveMsg();
        if(password == null) {
            this.close();
            return ;
        }

        try {
            this.userService.signUp(username, password);
            this.user = new User(null, username, password);
            sendMsg("Successful!");
            sendMsg("EXIT");
        } catch (UserAlreadyExistException ex) {
            sendMsg(ex.getMessage());
            sendMsg("EXIT");
        }
    }

    public void close() {
        try {
            if (this.clientSocket != null) {
                this.clientSocket.close();
            }
            if(this.in != null) {
                this.in.close();
            }
            if(this.out != null) {
                this.out.close();
            }
        } catch (IOException ex) {
            System.err.println("EXCEPTION: " + ex.getMessage());
        }
    }

    private String receiveMsg() {
        try {
            return (in.readLine());
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            this.close();
        }
        return (null);
    }

    private void sendMsg(String message) {
        try {
            this.out.write(message + "\n");
            this.out.flush();
        } catch (IOException ex) {
            System.err.println("EXCEPTION: " + ex.getMessage());
            this.close();
        }
    }

}
