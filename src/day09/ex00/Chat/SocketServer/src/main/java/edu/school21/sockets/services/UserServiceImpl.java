package edu.school21.sockets.services;

import edu.school21.sockets.exceptions.UserAlreadyExistException;
import edu.school21.sockets.models.User;
import edu.school21.sockets.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component("userService")
public class UserServiceImpl implements UserService {

    private PasswordEncoder passwordEncoder;
    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(@Qualifier("userRepository") UserRepository userRepository,
                          @Qualifier("passwordEncoder") PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void signUp(String login, String password) throws UserAlreadyExistException {
        if(this.isUserExist(login)) {
            throw new UserAlreadyExistException("ERROR: Login " + "\"" + login + "\"" + " already exist");
        }

        String encodedPassword = passwordEncoder.encode(password);

        userRepository.save(new User(null, login, encodedPassword));
    }

    private boolean isUserExist(String login) {
        return (userRepository.findByLogin(login)
                .isPresent());
    }
}
