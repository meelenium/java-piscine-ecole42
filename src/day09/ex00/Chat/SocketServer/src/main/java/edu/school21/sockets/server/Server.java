package edu.school21.sockets.server;

import edu.school21.sockets.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.ServerSocket;

@Component("server")
public class Server {

    private ServerSocket serverSocket;
    private ClientSocket clientSocket;
    private UserService userService;

    @Autowired
    public Server(@Qualifier("userService") UserService userService) {
        this.userService = userService;
    }

    public void start(int port) {
        try {
            this.serverSocket = new ServerSocket(port);
            this.clientSocket = new ClientSocket(this.serverSocket.accept(), this.userService);
            this.clientSocket.start();
        } catch (IOException ex) {
            System.err.println("EXCEPTION: " + ex.getMessage());
            this.close();
        }
    }

    private void close() {
        try {
            if (this.serverSocket != null) {
                this.serverSocket.close();
            }
        } catch (IOException ex) {
            System.err.println("EXCEPTION: " + ex.getMessage());
        }
        if(clientSocket != null) {
            this.clientSocket.close();
        }
        System.exit(0);
    }

}
