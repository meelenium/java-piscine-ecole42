package edu.school21.sockets.client;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    BufferedReader in;
    BufferedWriter out;
    Socket socket;

    public Client(String host, int port) {
        try {
            this.socket = new Socket(host, port);
            this.in = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
            this.out = new BufferedWriter(new OutputStreamWriter(this.socket.getOutputStream()));
        } catch (IOException ex) {
            System.err.println("EXCEPTION: " + ex.getMessage());
            this.close();
        }
    }

    public void start() {
        while(true) {
            this.receiveMsg();
            this.sendMsg();
        }
    }

    private void receiveMsg() {
        StringBuilder msg = new StringBuilder();
        String tmp;

        try {
            while((tmp = in.readLine()) != null) {
                    if(tmp.equals("END")) {
                        break ;
                    } else if(tmp.equals("EXIT")) {
                        this.close();
                    }
                    msg.append(tmp);
                    System.out.println(msg);
                    msg.setLength(0);
            }
        } catch (IOException ex) {
            System.err.println("EXCEPTION: " + ex.getMessage());
            this.close();
        }
    }

    private void sendMsg() {
        Scanner scanner = new Scanner(System.in);
        String msg;

        msg = scanner.nextLine();
        try {
            this.out.write(msg + "\n");
            this.out.flush();
        } catch (IOException ex) {
            System.err.println("EXCEPTION: " + ex.getMessage());
        }
    }

    private void close() {
        try {
            if (this.in != null) {
                this.in.close();
            }
            if(this.out != null) {
                this.out.close();
            }
            if(this.socket != null) {
                this.socket.close();
            }
        } catch (IOException ex) {
            System.err.println("EXCEPTION: " + ex.getMessage());
        }
        System.exit(0);
    }

}
