package edu.school21.sockets.app;

import edu.school21.sockets.client.Client;

public class Main {

    private static final String HOST = "localhost";

    public static void main(String[] args) {
        if(args.length != 1) {
            System.err.println("Argument error");
            System.exit(-1);
        }

        int port = parseArg("--server-port=", args[0]);
        Client client = new Client("localhost", port);

        client.start();
    }

    private static int parseArg(String startsWith, String arg) {
        if(!arg.startsWith(startsWith)) {
            System.err.println("Argument error");
            System.exit(-1);
        }

        return (Integer.parseInt(arg.substring(startsWith.length(), arg.length() - 1)));
    }

}
