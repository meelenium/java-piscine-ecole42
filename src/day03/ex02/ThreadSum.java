package day03.ex02;

public class ThreadSum extends Thread {
    private final int[] arr;
    private final Integer start;
    private final Integer end;
    private Integer sum;

    public ThreadSum(int[] arr, int start, int end) {
        this.arr = arr;
        this.start = start;
        this.end = end;
        this.sum = 0;
    }

    @Override
    public void run() {
        for(Integer number : arr) {
            sum += number;
        }
        this.printSum();
    }

    public Integer getSum() {
        return (sum);
    }

    private void printSum() {
        System.out.println(currentThread().getName() + ": "
                        + "from " + start
                        + " to " + (end - 1)
                        + " sum is " + sum);
    }
}
