package day03.ex02;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        if(args.length != 2) {
            System.err.println("Error: Argument error");
            System.exit(-1);
        }

        int arraySize = parseArguments(args[0], "--arraySize=");
        int countOfThreads = parseArguments(args[1], "--threadsCount=");

        if(arraySize > 2000000 || countOfThreads > arraySize) {
            System.err.println("Error: Argument error");
            System.exit(-1);
        }

        int start = 0;
        int stepSize = arraySize % countOfThreads == 0 ? arraySize / countOfThreads : arraySize / countOfThreads + 1;
        int end = stepSize;
        int sumOfThreads = 0;
        int[] arr = generateRandomArr(arraySize);
        List<ThreadSum> listOfThreads = new ArrayList<>();

        System.out.println("Sum: " + sumOfArray(arr));

        for(int i = 0; i < countOfThreads - 1; ++i) {
            listOfThreads.add(new ThreadSum(Arrays.copyOfRange(arr, start, end), start, end));
            start += stepSize;
            end += stepSize;
        }
        listOfThreads.add(new ThreadSum(Arrays.copyOfRange(arr, start, arraySize), start, arraySize));

        for(ThreadSum thread : listOfThreads) {
                thread.start();
        }

        for(ThreadSum thread : listOfThreads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        for(ThreadSum thread : listOfThreads) {
            sumOfThreads += thread.getSum();
        }

        System.out.println("Sum by threads: " + sumOfThreads);
    }

    public static int[] generateRandomArr(int arraySize) {
        int[] arr = new int[arraySize];
        Random randomInt = new Random();

        for(int i = 0; i < arraySize; ++i) {
            arr[i] = randomInt.nextInt(2001) - 1000;
        }
        return (arr);
    }

    public static int sumOfArray(int[] arr) {
        int sum = 0;
        for(int i = 0; i < arr.length; ++i) {
            sum += arr[i];
        }
        return (sum);
    }

    public static Integer parseArguments(String args, String keyWord) {
        if(!args.startsWith(keyWord)) {
            System.err.println("Error: Argument error");
            System.exit(-1);
        }
        return (Integer.parseInt(args.substring(args.indexOf('=') + 1)));
    }
}
