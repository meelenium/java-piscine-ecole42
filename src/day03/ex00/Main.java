package day03.ex00;

public class Main {
    public static void main(String[] args) {
        int countOfPrints = getCountOfPrints(args);
        EggHen egg = new EggHen("Egg", countOfPrints);
        EggHen hen = new EggHen("Hen", countOfPrints);

        egg.start();
        hen.start();
        try {
            hen.join();
            egg.join();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        for(int i = 0; i < 50; ++i) {
            System.out.println("Human");
        }
    }

    public static Integer getCountOfPrints(String[] args) {
        if(args.length != 1 || !args[0].startsWith("--count=")) {
            System.err.println("Error: Argument error");
            System.exit(-1);
        }
        return (Integer.parseInt(args[0].substring(args[0].indexOf('=') + 1)));
    }
}