package day03.ex00;

public class EggHen extends Thread {

    private String message;
    private Integer countOfPrints;

    public EggHen(String message, int countOfPrints) {
        this.countOfPrints = countOfPrints;
        this.message = message;
    }

    @Override
    public void run() {
        for(int i = 0; i < this.countOfPrints; ++i) {
            this.printMessage();
        }
    }

    private void printMessage() {
        System.out.println(this.message);
    }
}
