package day03.ex03;

import java.io.File;

public class Main {

    private final static String FILE_NAME = "src/day03/ex03/files_urls.txt";
    private final static String PATH_TO_SAVE = "src/day03/ex03/files/";

    public static void main(String[] args) {
        if(args.length != 1) {
            System.err.println("Error: Argument error");
            System.exit(-1);
        }

        File file = new File(FILE_NAME);
        LinkStorage linkStorage = new LinkStorage(file);

        int threadsCount = parseArguments(args[0], "--threadsCount=");

        if(threadsCount < 1) {
            System.err.println("Error: Argument error");
            System.exit(-1);
        }

        for(int i = 0; i < threadsCount; ++i) {
            new ThreadDownloader(linkStorage, PATH_TO_SAVE).start();
        }
    }

    public static Integer parseArguments(String args, String keyWord) {
        if(!args.startsWith(keyWord)) {
            System.err.println("Error: Argument error");
            System.exit(-1);
        }
        return (Integer.parseInt(args.substring(args.indexOf('=') + 1)));
    }
}
