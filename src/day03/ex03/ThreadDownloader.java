package day03.ex03;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class ThreadDownloader extends Thread {

    private LinkStorage linkStorage;
    private String pathToSave;

    public ThreadDownloader(LinkStorage linkStorage, String pathToSave) {
        this.linkStorage = linkStorage;
        this.pathToSave = pathToSave;
    }

    @Override
    public void run() {
        this.linkDownloader();
    }

    public void linkDownloader() {
            String link;
            URL url;

            for(int i = 0; i < 50; ++i) {
                link = linkStorage.getLinkFromList();
                if(link == null) {
                    break ;
                }
                try {
                    System.out.println(Thread.currentThread().getName() + " start download file number "
                                                                        + this.linkStorage.getIndex(link));
                    url = new URL(link);
                    ReadableByteChannel rbc = Channels.newChannel(url.openStream());
                    FileOutputStream fos = new FileOutputStream(this.pathToSave
                                                                + link.substring(link.lastIndexOf('/') + 1));
                    fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
                    fos.close();
                    rbc.close();
                    System.out.println(Thread.currentThread().getName() + " finish download file number "
                            + this.linkStorage.getIndex(link));
                } catch (IOException e) {
                    throw new RuntimeException(Thread.currentThread().getName() + ": download file error. File number - "
                                                                                + (linkStorage.getIndex(link) + 1));
                }
            }
    }

}
