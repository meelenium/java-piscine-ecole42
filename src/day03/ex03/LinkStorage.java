package day03.ex03;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class LinkStorage {
    private List<String> listOfLinks;
    private Iterator<String> iterator;

    public LinkStorage(File file) {
        this.listOfLinks = new ArrayList<>();
        this.getLinksFromFile(file);
        this.iterator = listOfLinks.iterator();
    }

    public List<String> getListOfLinks() {
        return (this.listOfLinks);
    }

    public synchronized String getLinkFromList() {
        if(iterator.hasNext()) {
            return (iterator.next());
        }
        return (null);
    }

    public synchronized int getIndex(String elem) {
        return (this.listOfLinks.indexOf(elem));
    }

    private void getLinksFromFile(File file) {
        try {
            Scanner scanner = new Scanner(file);
            while(scanner.hasNextLine()) {
                listOfLinks.add(scanner.nextLine());
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
