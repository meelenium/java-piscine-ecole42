package day03.ex01;

public class Printer {

    public synchronized void print(String message) {
        System.out.println(message);
        this.notify();
        try {
            this.wait();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
