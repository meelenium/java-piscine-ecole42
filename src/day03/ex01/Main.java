package day03.ex01;

public class Main {
    public static void main(String[] args) {
        int countOfPrints = getCountOfPrints(args);
        Printer printer = new Printer();
        EggHen egg = new EggHen("EGG", printer, countOfPrints);
        EggHen hen = new EggHen("HEN", printer, countOfPrints);
        Thread eggT = new Thread(egg);
        Thread henT = new Thread(hen);

        eggT.start();
        henT.start();

        try {
            eggT.join();
            henT.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static Integer getCountOfPrints(String[] args) {
        if(args.length != 1 || !args[0].startsWith("--count=")) {
            System.err.println("Error: Argument error");
            System.exit(-1);
        }
        return (Integer.parseInt(args[0].substring(args[0].indexOf('=') + 1)));
    }
}
