package day03.ex01;

public class EggHen implements Runnable {

    private final String message;
    private final Printer printer;
    private final Integer count;

    public EggHen(String message, Printer printer, int count) {
        this.count = count;
        this.printer = printer;
        this.message = message;
    }

    @Override
    public void run() {
        for(int i = 0; i < count; ++i) {
            printer.print(message);
        }
        synchronized (this.printer) {
            printer.notify();
        }
    }
}
