package edu.school21.service.application;

import edu.school21.service.models.User;
import edu.school21.service.repositories.UserRepository;
import edu.school21.service.repositories.UserRepositoryJdbcImpl;
import edu.school21.service.repositories.UserRepositoryJdbcTemplateImpl;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("context.xml");

        UserRepository userRepository = context.getBean("userRepositoryJdbcHikari", UserRepositoryJdbcImpl.class);

        for(int i = 0; i < 20; ++i) {
            userRepository.save(new User((long)i + 1, "testmail_" + (i + 1) + "@school21.ru"));
        }

        System.out.println("UserRepositoryJdbcImpl through HikariCP test");
        System.out.println("--------------------------------------------");

        System.out.println("findById test(id = 6): ");
        System.out.println(userRepository.findById(6L));
        System.out.println();

        System.out.println("findAll Test: ");
        System.out.println(userRepository.findAll());
        System.out.println();

        System.out.println("update test(will update user with id = 6): ");
        System.out.println("Old data: " + userRepository.findById(6L));
        userRepository.update(new User(6L, "test_update_mail@school21.ru"));
        System.out.println("New data: " + userRepository.findById(6L));
        System.out.println();

        System.out.println("delete test(will delete user with id = 6): ");
        System.out.println("Old data: " + userRepository.findById(6L));
        userRepository.delete(6L);
        System.out.println("New data: " + userRepository.findById(6L));
        System.out.println();

        System.out.println("findByEmailTest(user with email = testmail_2@school21.ru): ");
        System.out.println(userRepository.findByEmail("testmail_2@school21.ru"));
        System.out.println("--------------------------------------------");
        System.out.println();







        userRepository = context.getBean("userRepositoryJdbcDriverManager", UserRepositoryJdbcImpl.class);
        System.out.println("UserRepositoryJdbcImpl through DriverManager test");
        System.out.println("--------------------------------------------");

        System.out.println("findById test(id = 7): ");
        System.out.println(userRepository.findById(7L));
        System.out.println();

        System.out.println("findAll Test: ");
        System.out.println(userRepository.findAll());
        System.out.println();

        System.out.println("update test(will update user with id = 7): ");
        System.out.println("Old data: " + userRepository.findById(7L));
        userRepository.update(new User(7L, "test_update_mail@school21.ru"));
        System.out.println("New data: " + userRepository.findById(7L));
        System.out.println();

        System.out.println("delete test(will delete user with id = 7): ");
        System.out.println("Old data: " + userRepository.findById(7L));
        userRepository.delete(7L);
        System.out.println("New data: " + userRepository.findById(7L));
        System.out.println();

        System.out.println("findByEmailTest(user with email = testmail_3@school21.ru): ");
        System.out.println(userRepository.findByEmail("testmail_3@school21.ru"));
        System.out.println("--------------------------------------------");
        System.out.println();






        userRepository = context.getBean("userRepositoryJdbcTemplateHikari", UserRepositoryJdbcTemplateImpl.class);
        System.out.println("UserRepositoryJdbcTemplateImpl through HikariCP test");
        System.out.println("--------------------------------------------");

        System.out.println("findById test(id = 8): ");
        System.out.println(userRepository.findById(8L));
        System.out.println();

        System.out.println("findAll Test: ");
        System.out.println(userRepository.findAll());
        System.out.println();

        System.out.println("update test(will update user with id = 8): ");
        System.out.println("Old data: " + userRepository.findById(8L));
        userRepository.update(new User(8L, "test_update_mail@school21.ru"));
        System.out.println("New data: " + userRepository.findById(8L));
        System.out.println();

        System.out.println("delete test(will delete user with id = 8): ");
        System.out.println("Old data: " + userRepository.findById(8L));
        userRepository.delete(8L);
        System.out.println("New data: " + userRepository.findById(8L));
        System.out.println();

        System.out.println("findByEmailTest(user with email = testmail_4@school21.ru): ");
        System.out.println(userRepository.findByEmail("testmail_4@school21.ru"));
        System.out.println("--------------------------------------------");
        System.out.println();






        userRepository = context.getBean("userRepositoryJdbcTemplateDriverManager", UserRepositoryJdbcTemplateImpl.class);
        System.out.println("UserRepositoryJdbcTemplateImpl through DriverManager test");
        System.out.println("--------------------------------------------");

        System.out.println("findById test(id = 9): ");
        System.out.println(userRepository.findById(9L));
        System.out.println();

        System.out.println("findAll Test: ");
        System.out.println(userRepository.findAll());
        System.out.println();

        System.out.println("update test(will update user with id = 9): ");
        System.out.println("Old data: " + userRepository.findById(9L));
        userRepository.update(new User(9L, "test_update_mail@school21.ru"));
        System.out.println("New data: " + userRepository.findById(9L));
        System.out.println();

        System.out.println("delete test(will delete user with id = 9): ");
        System.out.println("Old data: " + userRepository.findById(9L));
        userRepository.delete(9L);
        System.out.println("New data: " + userRepository.findById(9L));
        System.out.println();

        System.out.println("findByEmailTest(user with email = testmail_5@school21.ru): ");
        System.out.println(userRepository.findByEmail("testmail_5@school21.ru"));
        System.out.println("--------------------------------------------");
        System.out.println();
    }

}
