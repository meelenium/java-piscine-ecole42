package edu.school21.spring.app;

import edu.school21.spring.interfaces.Printer;
import edu.school21.spring.printer.PrinterWithDateTimeImpl;
import edu.school21.spring.printer.PrinterWithPrefixImpl;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application.xml");
        Printer printer = context.getBean("printerWithPrefix", PrinterWithPrefixImpl.class);
        printer.print("hello world!");

        printer = context.getBean("printerWithDateTime", PrinterWithDateTimeImpl.class);
        printer.print("hello world!");
    }

}
