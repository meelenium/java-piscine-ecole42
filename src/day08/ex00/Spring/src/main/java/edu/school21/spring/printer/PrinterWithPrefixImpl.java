package edu.school21.spring.printer;

import edu.school21.spring.interfaces.Printer;
import edu.school21.spring.interfaces.Renderer;

public class PrinterWithPrefixImpl implements Printer {

    private Renderer renderer;
    private String prefix;

    public PrinterWithPrefixImpl(Renderer renderer) {
        this.renderer = renderer;
        this.prefix = null;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    @Override
    public void print(String msg) {
        renderer.rendererMsg(prefix + msg);
    }
}
