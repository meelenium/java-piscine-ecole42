package edu.school21.spring.interfaces;

public interface PreProcessor {

    String convertSymb(String str);

}
