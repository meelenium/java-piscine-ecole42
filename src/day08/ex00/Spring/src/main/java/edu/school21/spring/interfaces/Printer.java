package edu.school21.spring.interfaces;

public interface Printer {

    void print(String msg);

}
