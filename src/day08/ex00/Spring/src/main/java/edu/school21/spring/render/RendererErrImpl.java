package edu.school21.spring.render;

import edu.school21.spring.interfaces.PreProcessor;
import edu.school21.spring.interfaces.Renderer;

public class RendererErrImpl implements Renderer {

    private PreProcessor preProcessor;

    public RendererErrImpl(PreProcessor preProcessor) {
        this.preProcessor = preProcessor;
    }

    @Override
    public void rendererMsg(String msg) {
        System.err.println(preProcessor.convertSymb(msg));
    }
}
