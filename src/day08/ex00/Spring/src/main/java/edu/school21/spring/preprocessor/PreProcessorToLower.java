package edu.school21.spring.preprocessor;

import edu.school21.spring.interfaces.PreProcessor;

public class PreProcessorToLower implements PreProcessor {

    @Override
    public String convertSymb(String str) {
        return (str.toLowerCase());
    }
}
