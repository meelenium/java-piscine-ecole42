package edu.school21.spring.render;

import edu.school21.spring.interfaces.PreProcessor;
import edu.school21.spring.interfaces.Renderer;

public class RendererStandardImpl implements Renderer {

    private PreProcessor preProcessor;

    public RendererStandardImpl(PreProcessor preProcessor) {
        this.preProcessor = preProcessor;
    }

    @Override
    public void rendererMsg(String msg) {
        System.out.println(preProcessor.convertSymb(msg));
    }
}
