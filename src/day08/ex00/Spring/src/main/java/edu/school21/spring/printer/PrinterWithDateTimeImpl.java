package edu.school21.spring.printer;

import edu.school21.spring.interfaces.Printer;
import edu.school21.spring.interfaces.Renderer;
import java.time.LocalDateTime;

public class PrinterWithDateTimeImpl implements Printer {

    private Renderer renderer;
    private LocalDateTime localDateTime;

    public PrinterWithDateTimeImpl(Renderer renderer) {
        this.renderer = renderer;
        this.localDateTime = LocalDateTime.now();
    }

    @Override
    public void print(String msg) {
        renderer.rendererMsg(localDateTime.toString() + " " + msg);
    }
}
