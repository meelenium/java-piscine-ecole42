package edu.school21.service.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
@PropertySource("classpath:db.properties")
public class ApplicationConfig {

    @Value("${db.driver}")
    private String classPath;

    @Value("${db.url}")
    private String dbUrl;

    @Value("${db.user}")
    private String username;

    @Value("${db.password}")
    private String password;

    @Bean
    public HikariConfig hikariConfig() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(this.classPath);
        hikariConfig.setJdbcUrl(this.dbUrl);
        hikariConfig.setUsername(this.username);
        hikariConfig.setPassword(this.password);
        return (hikariConfig);
    }

    @Bean("hikariDS")
    public HikariDataSource hikariDataSource() {
        return (new HikariDataSource(this.hikariConfig()));
    }

    @Bean("driveManagerDS")
    public DriverManagerDataSource driverManagerDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(this.classPath);
        dataSource.setUrl(this.dbUrl);
        dataSource.setUsername(this.username);
        dataSource.setPassword(this.password);
        return (dataSource);
    }
    
}
