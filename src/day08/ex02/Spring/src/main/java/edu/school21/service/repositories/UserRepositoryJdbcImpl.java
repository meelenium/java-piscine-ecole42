package edu.school21.service.repositories;

import edu.school21.service.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component("userRepositoryJdbc")
public class UserRepositoryJdbcImpl implements UserRepository {

    private final String FIND_BY_ID_QUERY = "SELECT * " +
                                            "FROM \"user\" " +
                                            "WHERE id = ?";

    private final String FIND_ALL_QUERY = "SELECT * " +
                                          "FROM \"user\"";

    private final String SAVE_QUERY = "INSERT INTO \"user\"(email, password)" +
                                      "VALUES(?, ?)";

    private final String UPDATE_QUERY = "UPDATE \"user\" " +
                                        "SET email = ?, password = ? " +
                                        "WHERE id = ?";

    private final String DELETE_QUERY = "DELETE FROM \"user\" " +
                                        "WHERE id = ?";

    private final String FIND_BY_EMAIL_QUERY = "SELECT *" +
                                               "FROM \"user\" " +
                                               "WHERE email = ? " +
                                               "LIMIT 1";

    private DataSource dataSource;

    @Autowired
    public UserRepositoryJdbcImpl(@Qualifier("hikariDS") DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public User findById(Long ID) {
        try(Connection connection = this.dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID_QUERY)) {
            preparedStatement.setLong(1, ID);

            ResultSet resultSet = preparedStatement.executeQuery();
            if(!resultSet.next()) {
                return (null);
            }
            return (new User(resultSet.getLong(1),
                    resultSet.getString(2), resultSet.getString(3)));
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public List<User> findAll() {
        try(Connection connection = this.dataSource.getConnection();
            Statement statement = connection.createStatement()) {
            List<User> users = new ArrayList<>();

            ResultSet resultSet = statement.executeQuery(FIND_ALL_QUERY);
            while(resultSet.next()) {
                users.add(new User(resultSet.getLong(1),
                        resultSet.getString(2), resultSet.getString(3)));
            }

            return (users);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public void save(User entity) {
        try(Connection connection = this.dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SAVE_QUERY)) {

            preparedStatement.setString(1, entity.getEmail());
            preparedStatement.setString(2, entity.getPassword());

            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void update(User entity) {
        try(Connection connection = this.dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_QUERY)) {

            preparedStatement.setString(1, entity.getEmail());
            preparedStatement.setString(2, entity.getPassword());
            preparedStatement.setLong(3, entity.getId());

            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void delete(Long ID) {
        try(Connection connection = this.dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_QUERY)) {

            preparedStatement.setLong(1, ID);

            preparedStatement.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public Optional<User> findByEmail(String email) {
        try(Connection connection = this.dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_EMAIL_QUERY)) {

            preparedStatement.setString(1, email);

            ResultSet resultSet = preparedStatement.executeQuery();
            if(!resultSet.next()) {
                return (Optional.empty());
            }
            return (Optional.of(new User(resultSet.getLong(1),
                    resultSet.getString(2), resultSet.getString(3))));
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return (Optional.empty());
    }
}
