package edu.school21.service.repositories;

import edu.school21.service.models.User;
import java.util.Optional;

public interface UserRepository extends CrudRepository<User> {

    Optional<User> findByEmail(String email);

}
