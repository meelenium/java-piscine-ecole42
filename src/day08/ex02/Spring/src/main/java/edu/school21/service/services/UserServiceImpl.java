package edu.school21.service.services;

import edu.school21.service.models.User;
import edu.school21.service.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Random;

@Component("userService")
public class UserServiceImpl implements UserService {

    private final int PASSWORD_LENGTH = 15;
    private final String CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789#!_-";

    private UserRepository userRepository;

    public UserServiceImpl(@Qualifier("userRepositoryJdbcTemplate") UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public String signUp(String email) {
        if(Objects.isNull(email) || email.isEmpty()) {
            return (null);
        }

        String password = generateRandomPassword();
        User user = new User(null, email, password);

        userRepository.save(user);

        return (password);
    }

    private String generateRandomPassword() {
        StringBuilder stringBuilder = new StringBuilder();
        Random random = new Random();
        int indexOfChar;

        for(int i = 0; i < PASSWORD_LENGTH; ++i) {
            indexOfChar = random.nextInt(CHARS.length());
            stringBuilder.append(CHARS.charAt(indexOfChar));
        }

        return (stringBuilder.toString());
    }
}
