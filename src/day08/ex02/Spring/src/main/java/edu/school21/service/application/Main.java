package edu.school21.service.application;

import edu.school21.service.services.UserService;
import edu.school21.service.services.UserServiceImpl;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class Main {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("edu.school21.service");
        UserService userService = context.getBean("userService", UserServiceImpl.class);

        for(int i = 0; i < 20; ++i) {
            String email = new String("test_mail_" + (i + 1));
            String password = userService.signUp(email);
            System.out.println("Email: " + email
                                + "\n" + "password: " + password);
            System.out.println();
        }
    }
}
