package edu.school21.service.repositories;

import edu.school21.service.models.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;
import java.util.Optional;

@Component("userRepositoryJdbcTemplate")
public class UserRepositoryJdbcTemplateImpl implements UserRepository {

    private final String FIND_BY_ID_QUERY = "SELECT * " +
                                            "FROM \"user\" " +
                                            "WHERE id = ?";

    private final String FIND_ALL_QUERY = "SELECT * " +
                                          "FROM \"user\"";

    private final String SAVE_QUERY = "INSERT INTO \"user\"(email, password)" +
                                      "VALUES(?, ?)";

    private final String UPDATE_QUERY = "UPDATE \"user\" " +
                                        "SET email = ?, password = ? " +
                                        "WHERE id = ?";

    private final String DELETE_QUERY = "DELETE FROM \"user\" " +
                                        "WHERE id = ?";

    private final String FIND_BY_EMAIL_QUERY = "SELECT *" +
                                               "FROM \"user\" " +
                                               "WHERE email = ? " +
                                               "LIMIT 1";


    private JdbcTemplate jdbcTemplate;

    public UserRepositoryJdbcTemplateImpl(@Qualifier("hikariDS") DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public User findById(Long ID) {
        return (this.jdbcTemplate.query(FIND_BY_ID_QUERY, new Object[]{ID},
                        new BeanPropertyRowMapper<>(User.class))
                .stream()
                .findAny()
                .orElse(null));
    }

    @Override
    public List<User> findAll() {
        return (this.jdbcTemplate.query(FIND_ALL_QUERY,
                new BeanPropertyRowMapper<>(User.class)));
    }

    @Override
    public void save(User entity) {
        this.jdbcTemplate.update(SAVE_QUERY, entity.getEmail(), entity.getPassword());
    }

    @Override
    public void update(User entity) {
        this.jdbcTemplate.update(UPDATE_QUERY, entity.getEmail(), entity.getPassword(), entity.getId());
    }

    @Override
    public void delete(Long ID) {
        this.jdbcTemplate.update(DELETE_QUERY, ID);
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return (this.jdbcTemplate.query(FIND_BY_EMAIL_QUERY, new Object[]{email},
                        new BeanPropertyRowMapper<>(User.class))
                .stream()
                .findAny());
    }
}
