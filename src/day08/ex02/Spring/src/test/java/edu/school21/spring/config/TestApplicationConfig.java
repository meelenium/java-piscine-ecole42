package edu.school21.spring.config;

import edu.school21.service.repositories.UserRepository;
import edu.school21.service.repositories.UserRepositoryJdbcTemplateImpl;
import edu.school21.service.services.UserService;
import edu.school21.service.services.UserServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

@Configuration
public class TestApplicationConfig {

    @Bean("embeddedDSTest")
    @Scope("prototype")
    public EmbeddedDatabase getDataSource() {
        return(new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.HSQL)
                .build());
    }

    @Bean("userRepositoryTest")
    public UserRepository userRepository() {
        return (new UserRepositoryJdbcTemplateImpl(this.getDataSource()));
    }

    @Bean("userServiceTest")
    public UserService userService() {
        return (new UserServiceImpl(userRepository()));
    }

}
